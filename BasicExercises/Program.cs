﻿using System;

namespace BasicExercises
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            //BasicExercise 1
            //1.Write a C# Sharp program to print Hello and your name 
            //in a seperate line.

            Console.WriteLine("Hello!");
            Console.WriteLine("Hello Muzakkir.");


            //BasicExercises 2
            //2. Write a C# Sharp program to print the sum of two numbers.

            Console.WriteLine(15+17);
            //
            
            //BasicExercise 3
            //Write a C# Sharp program to print the result of dividing
            //two numbers.

            Console.WriteLine(36/6);
            //
        }
    }
}
  